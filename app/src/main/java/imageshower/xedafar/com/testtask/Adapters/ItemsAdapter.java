package imageshower.xedafar.com.testtask.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.filippudak.ProgressPieView.ProgressPieView;
import com.makeramen.roundedimageview.RoundedTransformationBuilder;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.util.List;

import imageshower.xedafar.com.testtask.Data.Item;
import imageshower.xedafar.com.testtask.Helpers;
import imageshower.xedafar.com.testtask.R;

/**
 * Created by Xedafar on 17.12.2017.
 */

public class ItemsAdapter extends BaseAdapter {
    private Context ctx;
    private LayoutInflater lInflater;
    private List<Item> objects;

    public ItemsAdapter(Context ctx, List<Item> objects) {
        this.ctx = ctx;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.objects = objects;
    }


    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int i) {
        return objects.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View viewItem, ViewGroup viewGroup) {
        View view = viewItem;
        if (view == null) {
            view = lInflater.inflate(R.layout.items_adapter_view, viewGroup, false);
        }
        Item object = (Item) getItem(i);
        Picasso.with(ctx).load(Helpers.getNotNullString(object.getItemImage())).into((ImageView) view.findViewById(R.id.item_image));
        ((TextView)view.findViewById(R.id.item_name)).setText(Helpers.getNotNullString(object.getItemName()));
        ((TextView)view.findViewById(R.id.item_price)).setText(String.format("%s$", Helpers.getNotNullString(object.getItemPrice())));
        ((TextView)view.findViewById(R.id.shop_name)).setText(Helpers.getNotNullString(object.getShopName()));
        ((TextView)view.findViewById(R.id.point_distance)).setText(Helpers.getNotNullString(Helpers.getNotNullString(object.getPointDistance())+" km"));
        final ProgressPieView progressPieView = view.findViewById(R.id.progressPieViewXml);
        Callback callback = new Callback() {
            @Override
            public void onSuccess() {
                progressPieView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onError() {

            }
        };
        progressPieView.setVisibility(View.INVISIBLE);
        Transformation transformation = new RoundedTransformationBuilder()
                .borderColor(Color.WHITE)
                .borderWidthDp(2)
                .cornerRadiusDp(45)
                .oval(false)
                .build();
        Picasso.with(ctx)
                .load(object.getShopLogo())
                .resize(120,120)
                .transform(transformation)
                .into((ImageView)view.findViewById(R.id.logo),callback);

        progressPieView.setProgress((int) object.getShopRank()/10);
        progressPieView.setProgressColor(ctx.getResources().getColor(R.color.colorGreen));



        return view;
    }

}
