package imageshower.xedafar.com.testtask.Data;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Xedafar on 17.12.2017.
 */

public interface Request {

    @GET("https://api.jeench.com/v1/search-items")
    Call<Model> getItems();
}
