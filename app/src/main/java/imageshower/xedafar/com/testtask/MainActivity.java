package imageshower.xedafar.com.testtask;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import butterknife.BindView;
import butterknife.ButterKnife;
import imageshower.xedafar.com.testtask.Adapters.ItemsAdapter;
import imageshower.xedafar.com.testtask.Data.Model;
import retrofit2.Call;
import retrofit2.Callback;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.items_list)
    ListView itemsList;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);//butterKnife bind
        context=this;

        App.getApi().getItems()
                .enqueue(new Callback<Model>() {
                    @Override
                    public void onResponse(Call<Model> call, retrofit2.Response<Model> response) {
                        ItemsAdapter itemsAdapter = new ItemsAdapter(context, response.body().getItem());
                        itemsList.setAdapter(itemsAdapter);

                    }

                    @Override
                    public void onFailure(Call<Model> call, Throwable t) {
                        t.toString();
                    }
                });


    }
}
