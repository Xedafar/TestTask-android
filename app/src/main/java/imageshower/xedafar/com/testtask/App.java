package imageshower.xedafar.com.testtask;

import android.app.Application;

import imageshower.xedafar.com.testtask.Data.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Xedafar on 17.12.2017.
 */

public class App extends Application {

    private static Request requests;
    private static Retrofit retrofit;

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.jeench.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        requests = retrofit.create(Request.class);


    }

    public static Request getApi() {
        return requests;
    }

}
